from tableau_file_extractor import TableauFileExtractor

tfe = TableauFileExtractor()

filters = {
    'Region':'Central',
    'Category':"Furniture,Office Supplies"
}

tfe.get_file('Superstore','Product', 'png', filters)