from typing import Dict
import tableauserverclient as TSC
import os

from dotenv import load_dotenv

class TableauObjectNotFoundException(Exception):
    pass

class TableauFileExtractor():

    def __init__(self):
        load_dotenv()
        self.tableau_auth = TSC.PersonalAccessTokenAuth(os.getenv("TOKEN_NAME"), os.getenv("TOKEN_VALUE"), '')
        self.server = TSC.Server(os.getenv("SERVER_URL"), use_server_version=True)
        self.server.auth.sign_in(self.tableau_auth)

    def get_file(self,workbook_name:str, view_name:str, file_type:str, view_filter_options:Dict={}):

        req_option = TSC.RequestOptions()
        req_option.filter.add(TSC.Filter(TSC.RequestOptions.Field.Name,
                                    TSC.RequestOptions.Operator.Equals,
                                    workbook_name))
        workbooks, pagination_item = self.server.workbooks.get(req_option)
        if len(workbooks) == 0: raise TableauObjectNotFoundException
        
        source_workbook = workbooks[0]
        self.server.workbooks.populate_views(source_workbook)

        workbook_views = [view for view in source_workbook.views if view.name == view_name]
        if len(workbook_views) == 0: raise TableauObjectNotFoundException

        source_view = workbook_views[0]

        view_filter_options = self._make_req_options(view_filter_options)

        if file_type == 'png':
            self.server.views.populate_image(source_view, view_filter_options)
            file_data = source_view.image
        elif file_type == 'pdf':
            self.server.views.populate_pdf(source_view, view_filter_options)
            file_data = source_view.pdf
        with open(f'images/{view_name}.{file_type}', 'wb') as f:
            f.write(file_data)
    
    def _make_req_options(self, filter_options:Dict={}) ->TSC.ImageRequestOptions:

        req_option = TSC.ImageRequestOptions()

        for option, option_value in filter_options.items():
            req_option.vf(option, option_value)
        
        return req_option